import Image from "next/image";
import React from "react";
import config from "../lib/config";

export function SocialList({}) {
  return (
    <div>
        <a
            title="Bio Devpand"
            href={`https://bio.devpand.com`}
            target="_blank"
            rel="noreferrer"
        >
        <figure className="navigationIconContainer">
            <Image src="/favicon.svg" alt="zyoruk icon" className="navigationIcon" width={24} height={24}/>
        </figure>
      </a>
      <a
        title="GitHub"
        href={`https://github.com/${config.github_account}`}
        target="_blank"
        rel="noreferrer"
      >
        <Image src="/github-alt.svg" alt="Github icon" width={24} height={24}/>
      </a>
      <a
        title="Gitlab"
        href={`https://gitlab.com/${config.gitlab_account}`}
        target="_blank"
        rel="noreferrer"
      >
        <Image src="/gitlab-alt.svg" alt="Github icon" width={24} height={24}/>
      </a>
      <style jsx>{`
        a {
          display: inline-block;
        }
        a:not(:last-child) {
          margin-right: 2em;
        }
        .navigationIconContainer{
            width: 24px;
            margin: 0;
        }
      `}</style>
    </div>
  );
}
