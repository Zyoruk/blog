import NextLink from "next/link";
import { Fragment } from "react";

interface Crumb {
    uri: string;
    name: string;
}
interface Props { 
    crumbs: Crumb[];
}
export const Breadcrumbs: React.FC<Props> = ({crumbs}) => {
    if (!crumbs) return null;

    const length = crumbs.length;
    const getBreadCrumbs = () => {
       return crumbs.map((crumb, index) => {
            if (index === length - 1){
                return <strong key={crumb.name}>{crumb.name}</strong>
            }
            return (
                <Fragment key={crumb.name}>
                    <NextLink href={crumb.uri} passHref>{crumb.name}</NextLink> /    
                </Fragment>
            );
        });
    }
    return (
      <div>
        {getBreadCrumbs()}
      </div>
    )
}