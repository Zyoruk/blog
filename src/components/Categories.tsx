import { TagContent } from "../lib/tags";
import TagLink from "./TagLink";

interface Props {
    tags: TagContent[]
}

export const Categories: React.FC<Props> = ({tags}) => {
    return (
        <>
            <ul className={"categories"}>
                {tags.map((it, i) => (
                    <li key={i}>
                        <TagLink tag={it} />
                    </li>
                ))}
            </ul>
            <style jsx>{`
                ul {
                    margin: 0;
                    padding: 0;
                }
                li {
                    list-style: none;
                }
                .categories {
                    display: none;
                }
                .categories li {
                    margin-bottom: 0.75em;
                }

                @media (min-width: 769px) {
                    .categories {
                        display: block;
                    }
                }
        `}</style>
        </>
    )
}