import "normalize.css";
import { AppProps } from "next/app";
import { Ubuntu, Tiro_Gurmukhi, Ubuntu_Mono, Roboto_Serif } from "next/font/google";
const ubuntu = Ubuntu({
  subsets: ["latin"],
  display: "swap",
  preload: true,
  weight: "300",
});

const tiro = Tiro_Gurmukhi({
  subsets: ["gurmukhi"],
  display: "swap",
  preload: true,
  weight: "400",
});

const ubuntuMono = Ubuntu_Mono({
  subsets: ["latin"],
  display: "swap",
  preload: true,
  weight: "400",
});

const robotoSerif = Roboto_Serif({
  subsets: ["latin"],
  display: "swap",
  preload: true,
  weight: "400",
});

// NOTE: Do not move the styles dir to the src.
// They are used by the Netlify CMS preview feature.
import "../../public/styles/global.css";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <style jsx global>{`
        :root {
          --ubuntu-font: ${ubuntu.style.fontFamily};
          --ubuntu-mono-font: ${ubuntuMono.style.fontFamily};
          --tiro-gurmukhi-font: ${tiro.style.fontFamily};
          --roboto-serif-font: ${robotoSerif.style.fontFamily};
        }
      `}</style>
      <Component {...pageProps} />
    </>
  );
}
