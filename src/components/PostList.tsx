import React from "react";
import { PostContent } from "../lib/posts";
import PostItem from "./PostItem";
import Pagination from "./Pagination";
import { TagContent } from "../lib/tags";
import { Categories } from "./Categories";
import { Breadcrumbs } from "./Breadcrumbs";

type Props = {
  posts: PostContent[];
  tags: TagContent[];
  pagination: {
    current: number;
    pages: number;
  };
};
export default function PostList({ posts, tags, pagination }: Props) {
  return (
    <div className={"container"}>
      <div className={"posts"}>
        <Breadcrumbs crumbs={[
                {
                    name: "All posts",
                    uri: "/posts"
                },
            ]
        }/>
        <ul className={"post-list"}>
          {posts.map((it, i) => (
            <li key={i}>
              <PostItem post={it} />
            </li>
          ))}
        </ul>
        <Pagination
          current={pagination.current}
          pages={pagination.pages}
          link={{
            href: (page) => (page === 1 ? "/posts" : "/posts/page/[page]"),
            as: (page) => (page === 1 ? null : "/posts/page/" + page),
          }}
        />
      </div>
      <Categories tags={tags}/>
      <style jsx>{`
        .container {
          display: flex;
          margin: 0 auto;
          max-width: 60rem;
          width: 100%;
          padding: 0 1.5rem;
        }
        ul {
          margin: 0;
          padding: 0;
        }
        li {
          list-style: none;
        }
        .posts {
          display: flex;
          flex-direction: column;
          flex: 1 1 auto;
        }
        .posts li {
          margin-bottom: 1.5rem;
        }
        .post-list {
          margin-top: 1.5rem;
          flex: 1 0 auto;
        }
      `}</style>
    </div>
  );
}
